<div dir="ltr" style="text-align: left;" trbidi="on">
Quick overview:<br />
This document is prepared by <b>DOD IT Solutions</b>, to give you an idea of how our MAKE MY TRIP <a href="https://www.doditsolutions.com/makemytrip-clone/">HOTEL CLONE SCRIPT</a> features would be.Hotel Clone Script is completely in built and has automated modules which are specific for online travel business. Online web based benefits of hotel room reservation process, create more sale leads and track customers, and most of all, it helps in maintaining your repeated customers info. You can analyze reservation trend details and work towards your future needs.<br />
<br />
<br />
UNIQUE FEATURES:<br />
Room/Inventory availability<br />
Real Time Pricing<br />
Real Time Booking/Reservation<br />
Online Cancellation/Refund<br />
Booking Amendments<br />
Travel Advisory Services<br />
Destination Management<br />
White Label Solutions<br />
Affiliate Model<br />
Multiple Payment Options<br />
Easy to manage Property Information<br />
Room Type , Inventory<br />
Room Rate Amenities<br />
Seasonal Rate and Other Charges<br />
<br />
COMMON&nbsp; FEATURES SEARCH OPTIONS<br />
User can search by entering location, check-in and check-out date along with the number of rooms and as well the number of adults/children whose going to reside.<br />
Map search is also available.<br />
<br />
FILTER OPTIONS<br />
User can check by selecting the stars (either 3 star hotels or 5 star hotels...etc)<br />
Check by filtering the pay scale ranging from the least amount to the highest.<br />
Can check by selecting a particular location.<br />
HOTEL&nbsp; INFO<br />
User can view the complete address of the selected hotel.<br />
Can view images and hotel amenities.<br />
Choose the particular room that you wish (Singe/Double/Luxury/Deluxe...etc).<br />
Access to API Hotels and Offer Hotels.<br />
<br />
MAKE MY TRIP HOTEL ADMIN PANEL LOGIN<br />
Log in using desired username and password&nbsp; (provided by admin)<br />
<br />
PROFILE<br />
View the Agent details<br />
Edit the agent details, block and unblock the agent record<br />
Change the login Password<br />
<br />
HOTEL MANAGEMENT<br />
Add the trip details<br />
Searched by trip details using by&nbsp; hotel type, from the city, and status<br />
List the&nbsp; trip details&nbsp; using on load Ajax functions<br />
Manage the trip details (Edit, delete, block and unblock, pagination)<br />
View and manage details<br />
View the particular hotel details<br />
structures designed by drag and drop method<br />
Can add/delete hotel details.<br />
Manage bookings and tickets list.<br />
Manage image uploads.<br />
Manage rooms.<br />
Manage user info.<br />
Maintain payment gateway and user wallet.<br />
Manage day’s offers.<br />
Complete end-to-end access.<br />
HOTEL IMAGES AND VIDEOS<br />
Add the hotel images<br />
Manage the hotel images(edit, status,delete)<br />
Upload the hotel videos<br />
<br />
PASSENGER MANAGEMENT<br />
List the booked traveler details<br />
View the particular guest details<br />
View the booking details<br />
View the particular count and view the hotel details<br />
Searched by hotel details using ticket no , booked date, user type, from city , to city<br />
<br />
SEAT MANAGEMENT<br />
Search and then list the seat details<br />
Searched by seat details using date<br />
<br />
TRAVELER MANAGEMENT<br />
List the all details<br />
View the particular traveler details<br />
Filtered by traveler details using date,from to city<br />
<br />
CANCEL<br />
List all traveler<br />
View the particular details<br />
Filtered by tickets using ticket no, cancel date, travel date<br />
<br />
PAYMENT MANAGEMENT<br />
List the hotel details<br />
View the particular payment transaction details<br />
Filtered by trip details using hotel type, from city, to city , date<br />
<br />
CANCELLATION POLICIES<br />
Add the refund status<br />
Manage the details(edit,status,delete)<br />
<br />
SMS LOG DETAILS<br />
List the sms details<br />
Manage the details(delete)<br />
Search sms details by bus and day,month,year<br />
<br />
EMAIL LOG DETAILS<br />
List the email details<br />
Manage the details(delete)<br />
Search details details by bus and day,month,year<br />
<br />
MANAGE BANNERS<br />
Banners to pop-up with offers would be generated.<br />
<br />
MANAGE MARQUEE TEXT<br />
Texts as flash notes either at the top or bottom of the page could be generated.<br />
<br />
Check Out Our Product in:<br />
<br />
<a href="https://www.doditsolutions.com/makemytrip-clone/">https://www.doditsolutions.com/makemytrip-clone/</a><br />
<a href="http://scriptstore.in/product/asp-net-makemytrip-clone/">http://scriptstore.in/product/asp-net-makemytrip-clone/</a><br />
<a href="http://phpreadymadescripts.com/shop/make-my-trip-hotel-clone-script.html">http://phpreadymadescripts.com/shop/make-my-trip-hotel-clone-script.html</a><br />
<div>
<br /></div>
</div>
